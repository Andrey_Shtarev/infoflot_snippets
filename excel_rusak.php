<?php
/* 20.10.2022 16∶12∶32
|--------------------------------------------------------------------------
| Задача № 119736
|--------------------------------------------------------------------------
|
| Выгрузить список всех агентов по офисам - Русаковская
| за послелние 2 года
| начиная с наиболее активных, и далее, на уменьшение
| Критерий: наибольшее кол-во заявок
|
*/

ini_set('memory_limit', '500M');
ini_set('error_reporting', E_ALL);  
ini_set('display_errors', 1);  
ini_set('display_startup_errors', 1);

$host     = '';
$dbname   = '';
$username = '';
$password = '';
$connection  = mysqli_connect($host, $username, $password, $dbname);
require_once __DIR__ . '/PHPExcel/Classes/PHPExcel.php';
require_once __DIR__ . '/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';

$connection->query("SET NAMES 'utf8'");
$connection->query("SET CHARACTER SET 'utf8'");
$connection->query("SET SESSION collation_connection = 'utf8_general_ci'");
$sql = "SELECT user_id, customer_id, request_customer_type, request_search_customer FROM ib_requests WHERE office_id = 27 AND request_date_create > 1634680800";

$result = $connection->query($sql)->fetch_all(MYSQLI_ASSOC);

$sql = "SELECT customer_id, customer_type, customer_name FROM ib_customers";
$customer_data = $connection->query($sql)->fetch_all(MYSQLI_ASSOC);

$customers = array();
foreach($customer_data as $value) {
    $customers[$value['customer_id']] = array();
    $customers[$value['customer_id']]['customer_type'] = $value['customer_type'];
    $customers[$value['customer_id']]['customer_name'] = $value['customer_name'];
}

unset($customer_data);

$customer_type = array(
    0 => 'Частное лицо',
    1 => 'Компания',
    2 => 'ИП',
    3 => 'Самозанятый'
);

$arr = [];
foreach($result as $key => $value) {

    if(!isset($arr[$value['user_id']]) && isset($customers[$value['customer_id']]) && $customers[$value['customer_id']]['customer_type'] != 0) {
        $arr[$value['user_id']] = array();

        $arr[$value['user_id']]['kolvo'] = 0;
        $arr[$value['user_id']]['customer_id'] = $value['customer_id'];
        $arr[$value['user_id']]['request_customer_type'] = $customer_type[$customers[$value['customer_id']]['customer_type']];
        $arr[$value['user_id']]['request_search_customer'] = $value['request_search_customer'];
        $arr[$value['user_id']]['customer_name'] = $customers[$value['customer_id']]['customer_name'];
    }

    if(isset($arr[$value['user_id']])) {
        $arr[$value['user_id']]['kolvo']++;
    }
}

arsort($arr);

// Создаем и подключаем объект
$xls = new PHPExcel();
$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet();

// Название документа
$sheet->setTitle('Русаковская');

$sheet->getColumnDimension("A")->setWidth(15);
$sheet->getStyle("A1")->getFont()->getColor()->setRGB('ff0000');
$sheet->getStyle("A1")->getFont()->setBold(true);
$sheet->setCellValue("A1", "Пользователь");

$sheet->getColumnDimension("B")->setWidth(15);
$sheet->getStyle("B1")->getFont()->getColor()->setRGB('ff0000');
$sheet->getStyle("B1")->getFont()->setBold(true);
$sheet->setCellValue("B1", "Плательщик");

$sheet->getColumnDimension("C")->setWidth(25);
$sheet->getStyle("C1")->getFont()->getColor()->setRGB('ff0000');
$sheet->getStyle("C1")->getFont()->setBold(true);
$sheet->setCellValue("C1", "Лицо");

$sheet->getColumnDimension("D")->setWidth(25);
$sheet->getStyle("D1")->getFont()->getColor()->setRGB('ff0000');
$sheet->getStyle("D1")->getFont()->setBold(true);
$sheet->setCellValue("D1", "Название");

$sheet->getColumnDimension("E")->setWidth(40);
$sheet->getStyle("E1")->getFont()->getColor()->setRGB('ff0000');
$sheet->getStyle("E1")->getFont()->setBold(true);
$sheet->setCellValue("E1", "Данные");

$sheet->getColumnDimension("F")->setWidth(10);
$sheet->getStyle("F1")->getFont()->getColor()->setRGB('ff0000');
$sheet->getStyle("F1")->getFont()->setBold(true);
$sheet->setCellValue("F1", "Кол-во");

$i = 2;
foreach($arr as $key => $value) {
    $sheet->setCellValue('A' . $i, $key);
    $sheet->setCellValue('B' . $i, $value['customer_id']);
    $sheet->setCellValue('C' . $i, $value['request_customer_type']);
    $sheet->setCellValue('D' . $i, $value['customer_name']);
    $sheet->setCellValue('E' . $i, trim($value['request_search_customer']));
    $sheet->setCellValue('F' . $i, $value['kolvo']);
    $i++;
}

// записываем в файл
$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save(__DIR__ . '/file.xlsx');