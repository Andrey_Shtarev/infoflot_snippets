<?php
/* 28.11.2022
|--------------------------------------------------------------------------
| Задача № 122533
|--------------------------------------------------------------------------
сделать выгрузку по базе пользователей.

1. Тип:  Агентство, Индивидуальный предприниматель, Самозанятый
2. Размер агентского вознаграждения (должны попасть все партнеры с типом комиссии ВЫШЕ стандартной (Стандарт не должно быть в отчете))
3. ИНН
4. Почта
5. Кол- во заявок 2021 год
6. Кол- во заявок 2022 год
7. Кол-во туристов 2021 год
8. Кол-во туристов 2022 год
9. Сумма оплат 2021 год
9. Сумма оплат 2022 год
10. Дата регистрации
*/

ini_set('memory_limit', '500M');
ini_set('error_reporting', E_ALL);  
ini_set('display_errors', 1);  
ini_set('display_startup_errors', 1);

$connect = mysqli_connect('','','','');
$connect->query("SET NAMES 'utf8'");
$connect->query("SET CHARACTER SET 'utf8'");
$connect->query("SET SESSION collation_connection = 'utf8_general_ci'");

if(!isset($_POST['schrit_2']) && !isset($_POST['schrit_3'])) {
    schrit_1($connect);
}

if(isset($_POST['schrit_2'])) {
    schrit_2($connect);
}

if(isset($_POST['schrit_3'])) {
    schrit_3();
}

function schrit_3() {
    require_once __DIR__ . '/PHPExcel/Classes/PHPExcel.php';
    require_once __DIR__ . '/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
    $j = 1;
    while($j < 3) {
        // Получаем массив
        $arr = unserialize(file_get_contents(__DIR__ . '/arr_202'. $j .'.txt'));
            
        // Сортируем по кол-ву заявок на убывание
        $array_name = array();
        foreach ($arr as $key => $row)
        {
        $array_name[$key] = $row['kolvo_zajavok'];
        }
        array_multisort($array_name, SORT_NUMERIC, SORT_DESC, $arr);
        unset($array_name);

        // Создаем и подключаем объект
        $xls = new PHPExcel();
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        // Название документа
        $sheet->setTitle('Данные за 202' . $j);

        $sheet->getColumnDimension("A")->setWidth(15);
        $sheet->getStyle("A1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("A1")->getFont()->setBold(true);
        $sheet->setCellValue("A1", "ID Пользователя");

        $sheet->getColumnDimension("B")->setWidth(15);
        $sheet->getStyle("B1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("B1")->getFont()->setBold(true);
        $sheet->setCellValue("B1", "Название");

        $sheet->getColumnDimension("C")->setWidth(25);
        $sheet->getStyle("C1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("C1")->getFont()->setBold(true);
        $sheet->setCellValue("C1", "Тип юрлица");

        $sheet->getColumnDimension("D")->setWidth(25);
        $sheet->getStyle("D1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("D1")->getFont()->setBold(true);
        $sheet->setCellValue("D1", "Тип комиссии");

        $sheet->getColumnDimension("E")->setWidth(15);
        $sheet->getStyle("E1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("E1")->getFont()->setBold(true);
        $sheet->setCellValue("E1", "ИНН");

        $sheet->getColumnDimension("F")->setWidth(10);
        $sheet->getStyle("F1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("F1")->getFont()->setBold(true);
        $sheet->setCellValue("F1", "Почта");

        $sheet->getColumnDimension("G")->setWidth(10);
        $sheet->getStyle("G1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("G1")->getFont()->setBold(true);
        $sheet->setCellValue("G1", " Кол-во заявок");

        $sheet->getColumnDimension("H")->setWidth(10);
        $sheet->getStyle("H1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("H1")->getFont()->setBold(true);
        $sheet->setCellValue("H1", "Кол-во туристов");

        $sheet->getColumnDimension("I")->setWidth(10);
        $sheet->getStyle("I1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("I1")->getFont()->setBold(true);
        $sheet->setCellValue("I1", "Сумма");

        $sheet->getColumnDimension("J")->setWidth(20);
        $sheet->getStyle("J1")->getFont()->getColor()->setRGB('ff0000');
        $sheet->getStyle("J1")->getFont()->setBold(true);
        $sheet->setCellValue("J1", "Дата регистрации");

        $i = 2;
        foreach($arr as $key => $value) {
            $sheet->setCellValue('A' . $i, $value['user_id']);
            $sheet->setCellValue('B' . $i, $value['name']);
            $sheet->setCellValue('C' . $i, $value['user_type']);
            $sheet->setCellValue('D' . $i, $value['commission_type']);
            $sheet->setCellValue('E' . $i, $value['tin']);
            $sheet->setCellValue('F' . $i, $value['user_email']);

            $sheet->setCellValue('G' . $i, $value['kolvo_zajavok']);
            $sheet->setCellValue('H' . $i, $value['kolvo_turistov']);
            $sheet->setCellValue('I' . $i, $value['summa']);
            $sheet->setCellValue('J' . $i, $value['user_date_registration']);
            $i++;
        }

        // записываем в файл
        $objWriter = new PHPExcel_Writer_Excel2007($xls);
        $objWriter->save(__DIR__ . '/file_202'. $j .'.xlsx');
        unlink(__DIR__ . '/arr_202'. $j .'.txt');
        $j++;
    }
    
}


function schrit_2($connect) {
    $arr = unserialize(file_get_contents(__DIR__ . '/arr.txt'));

    // тип комиссии
    $commission = $connect->query("SELECT * FROM ib_agency_commission_types");
    $commissionArray = $commission->fetch_all(MYSQLI_ASSOC);

    $commissionArr = array();
    foreach($commissionArray as $key => $value) {
        $commissionArr[$value['id']] = $value['title'];
    }
    unset($commissionArray);

    foreach($arr as $key => $value) {
        $commission_type_arr = explode(',', $value['commission_type']);
        $commission_types = '';
        foreach($commission_type_arr as $k => $v) {
            if(isset($commissionArr[$v])) {
                $commission_types = $commissionArr[$v] . ',' . $commission_types;
            }
        }

        $arr[$key]['commission_type'] = substr($commission_types, 0, -1);
    }

    if(isset($_POST['year'])) {
        year_2022($arr, $connect);
        return;
    }
    else {
        year_2021($arr, $connect);
        return;
    }
    $data = array('schrit_3' => 'schrit_3');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
        curl_exec($ch);
        curl_close($ch);
        return;
}

function schrit_1($connect) {
    $limit = 10000;
    $ab = 0;
    $str = '';
    $zahl = 0;
    
    if(isset($_POST['ab'])) {
        $ab = $_POST['ab'] + $limit;
        $str = $_POST['str'];
        $zahl = ++$_POST['zahl'];
    }
    
    $result = $connect->query("SELECT * FROM ib_users LIMIT $ab, $limit");
    
    $allArray = $result->fetch_all(MYSQLI_ASSOC);
    
    $arr = array();
    foreach($allArray as $key => $value) {
    
        $user_data = unserialize(base64_decode($value['user_data']));
    
        if(isset($user_data['commission_type']) && $user_data['commission_type'] != '' && $user_data['commission_type'] != '1' && $value['user_type'] != 0 && $value['user_type'] != 4) {
            
            if($value['user_type'] == 1) {
                $user_type = 'Агентство';
            }
            elseif($value['user_type'] == 2) {
                $user_type = 'Индивидуальный предприниматель';
            }
            elseif($value['user_type'] == 3) {
                $user_type = 'Самозанятый';
            }
            else {
                $user_type = $value['user_type'];
            }
    
            $user_legal_information = unserialize(base64_decode($value['user_legal_information']));
    
            $arr[$value['user_id'] . '_']['user_id'] = $value['user_id'];
            if($value['user_type'] == 1) {
                $arr[$value['user_id'] . '_']['name'] = $user_legal_information['company_name'];
            }
            else {
                $arr[$value['user_id'] . '_']['name'] = $user_data['first_name'] . ' ' . $user_data['middle_name'] . ' ' . $user_data['last_name'];
            }
            $arr[$value['user_id'] . '_']['user_type'] = $user_type;
            $arr[$value['user_id'] . '_']['commission_type'] = $user_data['commission_type'];
            $arr[$value['user_id'] . '_']['tin'] = unserialize(base64_decode($value['user_legal_information']))['tin'];
            $arr[$value['user_id'] . '_']['user_email'] = $value['user_email'];
            if($value['user_date_registration']) {
                $user_date_registration = date("d.m.Y", $value['user_date_registration']);
            }
            else {
                $user_date_registration = 'нет данных';
            }
            $arr[$value['user_id'] . '_']['user_date_registration'] = $user_date_registration;
            $arr[$value['user_id'] . '_']['requests'] = 0;
            $arr[$value['user_id'] . '_']['tourists'] = 0;
            $arr[$value['user_id'] . '_']['payments'] = 0;
            $arr[$value['user_id'] . '_']['year'] = 0;
    
        }
    }
    if($arr) {
        if($str != '') {
            $str = unserialize($str);
            $str = array_merge($str, $arr);
            $str = serialize($str);
            // $str = serialize(array_merge(unserialize($str), $arr));
        }
        else {
            $str = serialize($arr);
        }
        $data = array('ab' => $ab, 'str' => $str, 'zahl' => $zahl);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
        curl_exec($ch);
        curl_close($ch);
        file_put_contents(__DIR__ . '/result' . $zahl . '.txt', $str, FILE_APPEND | LOCK_EX);
        return;
    }
    else {
        file_put_contents(__DIR__ . '/arr.txt', $str, FILE_APPEND | LOCK_EX);  
        
        while($zahl >= 0) {
            $zahl--;
            unlink(__DIR__ . '/result' . $zahl . '.txt');
        }

        $data = array('schrit_2' => 'schrit_2');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
        curl_exec($ch);
        curl_close($ch);
        return;
    }
}

// Обработка следующего года (2022)
function weiter($year = 'year_2022') {
    $data = array('year' => $year, 'schrit_2' => 'schrit_2');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
    curl_exec($ch);
    curl_close($ch);
    exit;
}

// Кол-во заявок 2021 год
function year_2021($arr, $connect) {
    foreach($arr as $key => $value) {
        $res = $connect->query(
            "SELECT ib_requests.request_id, 
            ib_requests.request_date_create, 
            SUBSTRING_INDEX(SUBSTRING_INDEX(FROM_BASE64(ib_requests.request_prices),';',12),':',-1)  AS price, 
            ib_request_passengers.first_name
            FROM  `ib_requests` 
            LEFT JOIN  `ib_request_passengers` 
            ON  ib_requests.request_id = ib_request_passengers.request_id 
            WHERE ib_requests.user_id=" . $value['user_id'] . "
            AND ib_requests.request_date_create BETWEEN 1609455600 AND 1640991600"
        );
        $resArray = $res->fetch_all(MYSQLI_ASSOC);
    
        $arr[$key]['kolvo_turistov'] = count($resArray);
    
        $tempArr = array();
        foreach($resArray as $k => $v) {
            $tempArr[$v['request_id']] = $v['price'];
        }
    
        $arr[$key]['kolvo_zajavok'] = count($tempArr);
    
        $summa = 0;
        foreach($tempArr as $kk => $vv) {
            $summa += $vv;
        }
    
        $arr[$key]['summa'] = $summa;
    }
    file_put_contents(__DIR__ . '/arr_2021.txt', serialize($arr), FILE_APPEND | LOCK_EX); 
    weiter('year_2022');
    return;
}

//  Кол-во заявок 2022 год
function year_2022($arr, $connect) {
    foreach($arr as $key => $value) {
        $res = $connect->query(
            "SELECT ib_requests.request_id, 
            ib_requests.request_date_create, 
            SUBSTRING_INDEX(SUBSTRING_INDEX(FROM_BASE64(ib_requests.request_prices),';',12),':',-1)  AS price, 
            ib_request_passengers.first_name
            FROM  `ib_requests` 
            LEFT JOIN  `ib_request_passengers` 
            ON  ib_requests.request_id = ib_request_passengers.request_id 
            WHERE ib_requests.user_id=" . $value['user_id'] . " AND ib_requests.request_date_create > 1640991600"
        );
        $resArray = $res->fetch_all(MYSQLI_ASSOC);
    
        $arr[$key]['kolvo_turistov'] = count($resArray);
    
        $tempArr = array();
        foreach($resArray as $k => $v) {
            $tempArr[$v['request_id']] = $v['price'];
        }
    
        $arr[$key]['kolvo_zajavok'] = count($tempArr);
    
        $summa = 0;
        foreach($tempArr as $kk => $vv) {
            $summa += $vv;
        }
    
        $arr[$key]['summa'] = $summa;
    }
    file_put_contents(__DIR__ . '/arr_2022.txt', serialize($arr), FILE_APPEND | LOCK_EX);
    return;
}
